# Micropayment Field

Micropayment field provides functionality to configure a microprovider as a field value on an entity.

## Summary

* Provides a field type to store micropayment provider information.
* Provides a field widget so a user with access to modify the entity can add provider configuration.
* Provides a field formatter so that a user may pay through a provider to a person.

This does not provide functionality to create an e-commerce or accounting solution, but rather a site may offer its users the ability configure one or more micropayment providers for themselves. Or configure micropayment provider on entities such as nodes, taxonomy terms, etc... There are other modules that provide integration with a specific micropayment provider, but are usual aimed at one provider.

## Supported Providers

* Flattr
* PayPal (Buy Now)


## Similar Modules

* [Flattr](http://drupal.org/project/flattr): Similar use case and implementation, but only supports Flattr.
* [LM Paypal](http://drupal.org/project/lm_paypal): Similar implementation for embedding PayPal button, but different use case.

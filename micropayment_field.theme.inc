<?php
/**
 * @file
 * micropayment_field.theme.inc
 */


/**
 * Theme function for micropayment_field_link.
 */
function theme_micropayment_field_link(&$variables) {
  $info = micropayment_field_get_provider($variables['provider']);
  $options = array(
    'entity' => $variables['entity'],
    'entity_type' => $variables['entity_type'],
    'langcode' => $variables['langcode'],
    'field' => $variables['field'],
  );

  drupal_add_css(drupal_get_path('module', 'micropayment_field') . '/css/micropayment-field.css');

  try {
    $provider = new $info['handler']($options);
    return $provider->render($variables['value']);
  }
  catch (Exception $e) {
    watchdog_exception('micropayment_field', $e, '@message', array('@message' => $e->getMessage()), WATCHDOG_WARNING);
  }
}

<?php

/**
 * @file
 * Contains micropayment_field.api.php
 */

/**
 *
 * @return array
 *   An associative array of micropayment providers where the key is a machine
 *   name, and the value is an array with the following keys:
 *     - name: the machine name
 *     - label: translatable or string safe for markup
 *     - handler: a valid class that implements MicropaymentFieldInterface
 *     - path: the path of the handler
 *
 * @see \MicropaymentFieldInterface
 */
function hook_micropayment_field_provider_info() {
  return array(
    'my_provider' => array(
       'name' => 'my_provider',
       'label' => t('My Provider'),
       'handler' => 'MyProvider',
       'path' => drupal_get_path('module', 'my_provider_module') . '/includes/my_provider'
    )
  );
}

/**
 * Alter the micropayment field providers.
 *
 * @param array &$providers
 *   The array of providers from hook_micropayment_field_provider_info().
 */
function hook_micropayment_field_provider_info_alter(&$providers) {
  unset($providers['my_provider']);
}
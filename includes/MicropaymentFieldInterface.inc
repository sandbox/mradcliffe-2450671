<?php
/**
 * @file
 * MicropaymentFieldInterface.inc
 */

/**
 * Interface of methods for micropayment provider classes to support.
 */
interface MicropaymentFieldInterface {

  /**
   * Create a new instance of the class.
   *
   * @param $options
   *   An array of options to pass into the provider class including by default
   *   entity, entity_type, field, and langcode.
   */
  public function __construct(array $options);

  /**
   * Return a render array for a given provider and user name.
   *
   * @param $name
   *   The user name to build the provider widget for.
   * @return []
   *   A render array.
   */
  public function render($name);

  /**
   * Validate the user name for micropayment provider.
   *
   * @param $name
   *   The user name string to validate.
   * @return boolean
   *   TRUE if the user name is allowed.
   */
  public function validate($name);
}

<?php
/**
 * @file
 * MicropaymentFieldFlattr.inc
 */

/**
 * Flattr support.
 */
class MicropaymentFieldFlattr implements MicropaymentFieldInterface {

  static protected $langmap = array(
    'en' => 'en_US',
    'sq' => 'sq_AL',
    'ar' => 'ar_DZ',
    'be' => 'be_BY',
    'br' => 'br_FR',
    'bg' => 'bg_BG',
    'ca' => 'ca_ES',
    'zh-hans' => 'zh_CN',
    'zh-hant' => 'zh_CN',
    'hr' => 'hr_HR',
    'da' => 'da_DK',
    'nl' => 'nl_NL',
    'eo' => 'eo_EO',
    'et' => 'et_EE',
    'fi' => 'fi_FI',
    'fr' => 'fr_FR',
    'gl' => 'es_GL',
    'de' => 'de_DE',
    'el' => 'el_GR',
    'he' => 'iw_IL',
    'hi' => 'hi_IN',
    'hu' => 'hu_HU',
    'is' => 'is_IS',
    'id' => 'in_ID',
    'ga' => 'ga_IE',
    'it' => 'it_IT',
    'ja' => 'ja_JP',
    'ko' => 'ko_KR',
    'lv' => 'lv_LV',
    'mk' => 'mk_MK',
    'ml' => 'ms_MY',
    'mt' => 'mt_MT',
    'no' => 'no_NO',
    'nn' => 'nn_NO',
    'fa' => 'fa_FA',
    'pl' => 'pl_PL',
    'pt' => 'pt_PT',
    'ro' => 'ro_RO',
    'ru' => 'ru_RU',
    'sr' => 'sr_RS',
    'sk' => 'sk_SK',
    'sl' => 'sl_SI',
    'es' => 'es_ES',
    'sv' => 'sv_SE',
    'th' => 'th_TH',
    'tr' => 'tr_TR',
    'uk' => 'uk_UA',
    'vi' => 'vi_VN',
  );

  /**
   * {@inheritdoc}
   */
  public function __construct(array $options) {
    if (isset($options['langcode']) && isset(self::$langmap[$options['langcode']])) {
      $options['flattr']['language'] = self::$langmap[$options['langcode']];
    }

    $this->options = array(
      'flattr' => array(
        'title' => t('Flattr me'),
        'description' => t('Flattr me'),
        'language' => 'en_US',
        'tags' => array(),
        'category' => '',
        'hidden' => 0,
        'popout' => 1,
        'button' => 'compact',
      ),
    ) + $options;
  }

  /**
   * {@inheritdoc}
   */
  public function render($name) {
    $attributes = array(
      'title' => $this->options['flattr']['title'],
      'class' => array('FlattrButton'),
      'lang' => $this->options['flattr']['language'],
      'data-flattr-uid' => preg_replace('/[^a-zA-Z0-9\-_]/', '', $name),
      'data-flattr-hidden' => $this->options['flattr']['hidden'],
      'data-flattr-popout' => $this->options['flattr']['popout'],
    );

    if ($this->options['flattr']['tags']) {
      $attributes['data-flattr-tags'] = preg_replace('/[^a-zA-Z]/', '', implode(',', $this->options['flattr']['tags']));
    }

    if ($this->options['flattr']['category']) {
      $attributes['data-flattr-category'] = check_plain($this->options['flattr']['category']);
    }

    if ($this->options['flattr']['button']) {
      $attributes['data-flattr-button'] = check_plain($this->options['flattr']['button']);
    }

    $languages = language_list();
    $options = array(
      'attributes' => $attributes,
      'absolute' => TRUE,
      'language' => $languages[$this->options['langcode']],
      'html' => FALSE,
    );

    drupal_add_js('https://api.flattr.com/js/0.6/load.js?mode=auto', 'external');
    return l($this->options['flattr']['title'], current_path(), $options);
  }

  /**
   * {@inheritdoc}
   */
  public function validate($name) {
    // "Username can only contain the characters a-z, 0-9, _ and -.".
    if (preg_match('/[^a-zA-Z0-9_\-]/', $name)) {
      return FALSE;
    }

    return TRUE;
  }

}

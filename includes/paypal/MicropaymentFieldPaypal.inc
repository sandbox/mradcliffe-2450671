<?php
/**
 * @file
 * MicropaymentFieldFlatter.inc
 */


/**
 * Paypal support.
 *
 * @todo donate vs buynow
 */
class MicropaymentFieldPaypal implements MicropaymentFieldInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $options) {
    $this->options = $options + array(
      'paypal' => array(
        'type' => 'buynow',
        'currency_code' => 'USD',
        'no_note' => 0,
      ),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function render($name) {
    $form = array(
      '#type' => 'form',
      '#action' => 'https://www.paypal.com/cgi-bin/webscr',
      '#attributes' => array(
        'target' => '_blank',
      ),
    );

    $form['cmd'] = array('#type' => 'hidden', '#name' => 'cmd', '#value' => '_xclick');
    $form['business'] = array('#type' => 'hidden', '#name' => 'business', '#value' => $name);
    $form['lc'] = array('#type' => 'hidden', '#name' => 'lc', '#value' => strtoupper($this->options['langcode']));
    $form['button_subtype'] = array('#type' => 'hidden', '#name' => 'button_subtype', '#value' => 'services');
    $form['no_note'] = array('#type' => 'hidden', '#name' => 'no_note', '#value' => $this->options['paypal']['no_note']);
    $form['currency_code'] = array('#type' => 'hidden', '#name' => 'currency_code', '#value' => $this->options['paypal']['currency_code']);
    $form['bn'] = array('#type' => 'hidden', '#name' => 'bn', '#value' => 'PP-BuyNowBF:btn_paynow_SM.gif:NonHostedGuest');

    $form['submit'] = array(
      '#type' => 'image_button',
      '#src' => drupal_get_path('module', 'micropayment_field') . '/css/btn_paynow_SM.gif',
      '#attributes' => array(
        'border' => 0,
        'alt' => t('PayPal - The safer, easier way to pay online!'),
        'class' => array('PayPalButton'),
      ),
    );

    // <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

    return drupal_render($form);
  }

  /**
   * {@inheritdoc}
   */
  public function validate($name) {
    return valid_email_address($name);
  }
}

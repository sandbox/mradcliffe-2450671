<?php
/**
 * @file
 * micropayment_field.field.inc
 */


/**
 * Implements hook_field_info().
 */
function micropayment_field_field_info() {
  return array(
    'micropayment_field' => array(
      'label' => t('Micropayment'),
      'description' => t('Provides a field type to store micropayment provider information.'),
      'settings' => array(),
      'instance_settings' => array(
        'allowed_providers' => array(),
      ),
      'default_widget' => 'micropayment_field_settings',
      'default_formatter' => 'micropayment_field_link',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function micropayment_field_field_widget_info() {
  return array(
    'micropayment_field_settings' => array(
      'label' => t('Micropayment Settings'),
      'description' => t('Provide a widget for a user to configure micropayment providers.'),
      'settings' => array(),
      'field types' => array('micropayment_field'),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_DEFAULT,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_formatter_info().
 */
function micropayment_field_field_formatter_info() {
  return array(
    'micropayment_field_link' => array(
      'label' => t('Micropayment Link'),
      'description' => t('Provides a simple link to micropayment provider.'),
      'settings' => array(),
      'field types' => array('micropayment_field'),
    ),
  );
}

/**
 * Implements hook_field_settings_form().
 */
function micropayment_field_field_instance_settings_form($field, $instance) {
  $form['allowed_providers'] = array(
    '#type' => 'select',
    '#title' => t('Allowed Providers'),
    '#description' => t('Optionally restrict the list of providers for this field instance to the following selected options.'),
    '#options' => micropayment_field_provider_options(),
    '#default_value' => $instance['settings']['allowed_providers'],
    '#multiple' => TRUE,
    '#required' => FALSE,
  );

  return $form;
}

/**
 * Implements hook_field_validate().
 */
function micropayment_field_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  $provider = FALSE;
  $allowed_providers = $instance['settings']['allowed_providers'];
  if (empty($allowed_providers)) {
    $allowed_providers = array_keys(micropayment_field_provider_options());
  }

  foreach ($items as $delta => $item) {
    if (!in_array($item['provider'], $allowed_providers)) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'provider_not_allowed',
        'message' => t('%name: this micropayment provider is not allowed. Please choose another one.', array('%name' => $instance['label'])),
      );
    }
    else {
      $info = micropayment_field_get_provider($item['provider']);
      $provider = new $info['handler'](array());
    }

    // Validate the user name with its provider.
    if ($provider && !empty($item['value']) && !$provider->validate($item['value'])) {
      $errors[$field['field_name']][$langcode][$delta][] = array(
        'error' => 'illegal_value',
        'message' => t('%name: provide a valid user name for this micropayment provider.', array('%name' => $instance['label'])),
      );
    }
  }
}

/**
 * Implements hook_field_is_empty().
 */
function micropayment_field_field_is_empty($item, $field) {
  if (empty($item['value']) && (string) $item['value'] !== '0') {
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_field_widget_form().
 */
function micropayment_field_field_widget_form(&$form, &$form_state, $field, $instance, $langecode, $items, $delta, $element) {

  $allowed_providers = micropayment_field_provider_options($instance['settings']['allowed_providers']);

  $provider_element = array(
    '#type' => 'select',
    '#title' => t('Choose Provider'),
    '#options' => $allowed_providers,
    '#required' => TRUE,
    '#default_value' => isset($items[$delta]) ? $items[$delta]['provider'] : '',
    '#weight' => -10,
  );

  $element += array(
    '#type' => 'textfield',
    '#default_value' => isset($items[$delta]) && isset($items[$delta]['value']) ? $items[$delta]['value'] : '',
  );

  return array(
    'provider' => $provider_element,
    'value' => $element,
  );
}

/**
 * Implements hook_field_formatter_view().
 */
function micropayment_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ($langcode == LANGUAGE_NONE) {
    $language = language_default();
    $langcode = $language->language;
  }

  foreach ($items as $delta => $item) {
    $element[$delta] = array(
      '#theme' => 'micropayment_field_link',
      '#provider' => $item['provider'],
      '#value' => $item['value'],
      '#entity' => $entity,
      '#entity_type' => $entity_type,
      '#field' => $field,
      '#langcode' => $langcode,
    );
  }

  return $element;
}

<?php

/**
 * @file
 * micropayment_field.micropayment.inc
 */


/**
 * Implements hook_micropayment_field_provider_info().
 */
function micropayment_field_micropayment_field_provider_info() {
  $path = drupal_get_path('module', 'micropayment_field') . '/includes';
  return array(
      'flattr' => array(
        'name' => 'flattr',
        'label' => t('Flattr'),
        'handler' => 'MicropaymentFieldFlattr',
        'path' => $path . '/flattr',
      ),
      'paypal' => array(
        'name' => 'paypal',
        'label' => t('Paypal'),
        'handler' => 'MicropaymentFieldPaypal',
        'path' => $path . '/paypal',
      ),
  );
}
